# IntrospectData Deployment Library

This repository is a collection of 'random stuff' we've built, needed or otherwise wanted to share with others. It is not:

* A holistic set of tools to be used as-is without guidance
* Best practice for all situations
* Intended to be used without actually digging into it

Why do we put stuff here? Well... it could be anything, but here are a few reasons:

* We already had 80-90% of a 'solution' and figured it was useful so we thought we'd share it.
* By some circuitous route, we decided that putting it here for 'free' was easier than other routes to make it available.
* We had done something repeatedly and were tired of doing it over and over again. Once we figured out the details, it made sense to put it here because... well... why not?

## Ansible/Host-based Stuff

### [Wordpress on Amazon Linux 2](wp-alinux2/)

Do you want to deploy Wordpress to AWS and use EFS behind it for shared storage? Well do we have a project for you. This [Ansible](https://github.com/ansible/ansible)-based setup uses [Vagrant](https://vagrantup.com) for local development and [Packer](https://packer.io) to build an AMI that allows you to deploy you some Wordpress!
