# Wordpress via Ansible on Amazon Linux 2

What's included:

* Apache + PHP
* Wordpress (5.0.3 as of the writing of the latest commit)
* Some fine tuning for NFS use in serving the application on multiple hosts

## Configuration Variables

In the [configure.yml](configure.yml) playbook there are a number of configuration variables. They fall into several groups:

* Critical Inputs:
    * Wordpress Database information
    * Wordpress Configuration values (salts)
    * WP Host/HTTP Configurations
    * Wordpress File Package Download URL
    * NFS/EFS Mount path (should be in <mount_address>:/<path> format)
* Paths and mechanics for where things will be put/mounted, etc. and for setting permissions...
* Tweaks to PHP's configuration that are 'mostly static' but extracted to variables for the sake of potential convenience down the road

## Specific 'tweaks' included

* NFS tweaks for AWS' EFS service... as identified by:
    * https://blog.lawrencemcdaniel.com/tuning-aws-efs-for-wordpress/
* PHP Configurations to handle shared storage a bit more nicely. Taken loosely from:
    * https://aimeos.org/tips/modern-php-applications-up-to-110x-slower-on-network-storage/
    * https://alanthing.com/blog/2013/05/09/speed-up-php-on-nfs-with-turbo-realpath/
    * https://www.tecmint.com/install-opcache-in-centos-7/
    * https://tideways.com/profiler/blog/fine-tune-your-opcache-configuration-to-avoid-caching-suprises

## Using via Vagrant (locally)

**Prerequisites**

* [Vagrant](https://vagrantup.com) (For developing locally)
    * [VirtualBox](https://virtualbox.org)
* [Packer](https://packer.io) (For building AMI's in AWS)
* [Python](https://www.python.org/) - 2 or 3... doesn't really matter
    * [Virtualenv](https://virtualenv.pypa.io/en/latest/) and [Virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) - because they're good ideas and good pythonic practice
    * [pip](https://pip.pypa.io/en/stable/) - will be added to your virtualenv automatically
* Python Packages
    * [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-via-pip)
* Other niceties or things to make your life easier:
    * [Atom](https://atom.io) text editor ([installing packages](https://nearsoft.com/blog/how-to-install-packages-in-atom/))
        * [atom-beautify](https://atom.io/packages/atom-beautify)
        * [platformio-ide-terminal](https://atom.io/packages/platformio-ide-terminal)
* To test the NFS and MySQL connectivity portions of this setup, it's helpful to have those set up 'somewhere else'
    * I just set them up on my local NAS :)

```bash
vagrant up || true && vagrant provision
```

## Using... somewhere else.

This repo should be leveraged using the [`ansible-playbook`](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) command. Depending on 'where' you're running it you have a few options. Prior to doing any of them, you'll want to modify the `ansible_ssh_user` variable in both playbooks to the appropriate ssh user for access. Before you run this anywhere, make sure you've updated the variables in `/vars` to properly reflect your environment:

* db.yaml - set root db user/password, db host name and the user/password you want to create for wordpress to use
* nfs.yaml - set the NFS endpoint if applicable
* ssh.yaml - set ssh user and key details
* wordpress.yaml - set wordpress-level values and configurations


**Run it on the local machine**
Follow the details [here](https://www.tricksofthetrades.net/2017/10/02/ansible-local-playbooks/) to update the playbooks for local execution.

You'll also need to have ansible installed locally:

```bash
sudo yum install python python-pip
sudo pip install ansible
```

Then copy this directory up to the server (or grab it from gitlab [here](https://gitlab.com/introspectdata/public/deployment-library/-/archive/master/deployment-library-master.tar.gz), unpack it and get going)

```bash
ansible-playbook --connection=local 127.0.0.1 install.yaml
ansible-playbook --connection=local 127.0.0.1 configure.yaml
```

**Run it against remote hosts**
In order to do this you'll want to make you've got ssh keys all set up :)

```bash
ansible-playbook -i "comma,separated,host,or,ip," install.yaml
ansible-playbook -i "comma,separated,host,or,ip," configure.yaml
```

### As a bootstrap script

If you're using the AMI build or you're simply installing all at once, you'll want to overwrite the var files in the `vars/` directory:

* `vars/db.yaml` to reflect db root user and wordpress user to create
* `vars/nfs.yaml` to reflect endpoint for NFS mount
* `vars/ssh.yaml` to reflect the proper SSH user (likely `ec2-user`)
* `vars/wordpress.yaml` to reflect the proper keys and wordpress-specific configurations

## Building an AMI or other image

This directory also includes a `packer.json` file meant to build machine images via [packer](https://packer.io).

The build command for this, once packer is installed, is fairly simple:

* Set AWS-specific environment variables for API access:
    * `AWS_ACCESS_KEY_ID`
    * `AWS_SECRET_ACCESS_KEY`
    * `AWS_REGION`
* Run the following command:

```bash
packer build packer.json
```


## Known Issues/Oddities

### Vagrant Cannot Mount Local Path

You may see an error such as the following when initializing Vagrant:

```bash
Vagrant was unable to mount VirtualBox shared folders. This is usually

because the filesystem "vboxsf" is not available. This filesystem is
made available via the VirtualBox Guest Additions and kernel module.
Please verify that these guest additions are properly installed in the
guest. This is not a bug in Vagrant and is usually caused by a faulty
Vagrant box. For context, the command attempted was:

mount -t vboxsf -o uid=1000,gid=1000 vagrant /vagrant

The error output from the command was:

/sbin/mount.vboxsf: mounting failed with the error: No such device
```

First of all-don't worry. Second, Vagrant's likely already brought the box up, and since we don't really need that mount point anyway we're good. Go ahead and keep going with `vagrant provision` to actually apply our configuration.

## It's not perfect...

There are certainly a few things that we could clean up with this, but we aren't convinced it's worth the time:

* [x] pull all the localhost stuff for mysql into a ~role~ task list that's run as a specific chunk of work
* [ ] inject a few more sets of variables related to Wordpress best practices and configurations aimed at lowering the # of db calls that it makes (like the host/site entry)
* [ ] draw a pretty picture to show how it this thing would plug into an AWS architecture (though admittedly this will work with any NFS storage provider)
* [ ] maybe make a [chef](https://chef.io)-ey version of this to show the differences?
* [ ] do some more testing with it... because testing is always good
